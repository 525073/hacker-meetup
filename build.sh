raylib_path=../raylib
emcc -o ./public/index.html \
    $1 -Os -Wall ./libraylib.a \
    -I. -I $raylib_path/src \
    -L. -L $raylib_path/src \
    -s USE_GLFW=3 \
    -s ASYNCIFY \
    --preload-file resources \
    --shell-file ./shell.html \
    -s TOTAL_STACK=64MB \
    -s INITIAL_MEMORY=128MB \
    -s ASSERTIONS \
    -DPLATFORM_WEB
