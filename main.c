#include "raylib.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Define screen dimensions
#define SCREEN_WIDTH 3900
#define SCREEN_HEIGHT 3600
#define TRUE 1
#define FALSE 0
#define FILTER_POINT 0
#define LOGO_TEXT "MUNI CTF MEETUP"
#define LARGE_FONT_SIZE 50
#define MEDIUM_FONT_SIZE 37
#define SMALL_FONT_SIZE 40

#define HOVER_COLOR \
    (Color) { 0, 104, 0, 255 } // Green for hover state
#define NORMAL_COLOR \
    (Color) { 248, 248, 248, 255 } // White for normal state
#define PRESSED_COLOR \
    (Color) { 0, 230, 0, 255 } // Light green for pressed state

// Define menu options
typedef enum
{
    ABOUT,
    LOCATION,
    CONTACT,
    EVENTS,
    MENU_OPTIONS_COUNT
} MenuOption;

// Define menu option labels
const char *menuOptionsNames[MENU_OPTIONS_COUNT] = {
    "ABOUT",
    "LOCATION",
    "CONTACT",
    "EVENTS"};

const char *menuPopupName[MENU_OPTIONS_COUNT] = {
    "ABOUT",
    "WHERE WE ARE",
    "HOW TO CONTACT US",
    "WHAT'S HAPPENING"};

const char *menuPopupMessage[MENU_OPTIONS_COUNT] = {
    "- We are group of enthusiasts focused on cybersecurity.\n- We meet to solve HackTheBox challenges and TryHackMe rooms.\n- Anyone is welcome to join us.\n- You don't have to be a MUNI student to participate.",
    "Meetups usually take place at the \nFaculty of Informatics, Masaryk University, Brno.",
    "mail: tomci@ics.muni.cz.",
    "Meetups are held once a month. Contact us for more information."};

typedef struct
{
    int x;
    int y;
    const char *text;
    int clicked;
    int hover;
} MenuOpt;

// Define Game of Life grid size
#define GRID_SIZE 200
#define CELL_SIZE 10

int grid[GRID_SIZE][GRID_SIZE];
int nextGrid[GRID_SIZE][GRID_SIZE];

void drawMenuOption(MenuOpt menuOption, Font font, bool clicked);
int isClicked(int x, int y, int width, int height, int clicked);
void InitGameOfLife();
void UpdateGameOfLife();
void DrawGameOfLife();
void AddCellToGrid(int mouseX, int mouseY);
void DrawMenu(Font fonts[], MenuOpt menuOptions[], int *popup);

int main(void)
{
    InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Main Menu");
    SetTargetFPS(60);

    MenuOpt menuOptions[MENU_OPTIONS_COUNT];
    Font fonts[2];
    fonts[0] = LoadFontEx("resources/fonts/VT323-Regular.ttf", LARGE_FONT_SIZE, 0, 250);
    fonts[1] = LoadFontEx("resources/fonts/retroFont.ttf", LARGE_FONT_SIZE, 0, 250);

    InitGameOfLife();

    int popup = -1;
    while (!WindowShouldClose())
    {
        if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
        {
            AddCellToGrid(GetMousePosition().x, GetMousePosition().y);
        }

        UpdateGameOfLife();

        BeginDrawing();
        ClearBackground((Color){20, 20, 20, 255}); // Dark background

        DrawGameOfLife();
        DrawMenu(fonts, menuOptions, &popup);

        EndDrawing();
    }

    CloseWindow(); // Close window and OpenGL context
    return 0;
}

void drawMenuOption(MenuOpt menuOption, Font font, bool clicked)
{
    if (clicked)
    {
        menuOption.x += 35;
        DrawTextEx(font, menuOption.text, (Vector2){menuOption.x, menuOption.y}, MEDIUM_FONT_SIZE, 0, PRESSED_COLOR);
        return;
    }

    Color color = menuOption.hover ? HOVER_COLOR : NORMAL_COLOR;
    DrawTextEx(font, menuOption.text, (Vector2){menuOption.x, menuOption.y}, MEDIUM_FONT_SIZE, 0, color);
}

int isClicked(int x, int y, int width, int height, int clicked)
{
    if (CheckCollisionPointRec(GetMousePosition(), (Rectangle){x, y, width, height}))
    {
        if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
        {
            clicked = TRUE;
        }
    }
    return clicked;
}

void InitGameOfLife()
{
    int seed = time(NULL);
    srand(seed);

    for (int i = 0; i < GRID_SIZE; i++)
    {
        for (int j = 0; j < GRID_SIZE; j++)
        {
            grid[i][j] = rand() % 15 == 0 ? 1 : 0;
        }
    }
}

void UpdateGameOfLife()
{
    for (int i = 0; i < GRID_SIZE; i++)
    {
        for (int j = 0; j < GRID_SIZE; j++)
        {
            int liveNeighbors = 0;

            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    if (x == 0 && y == 0)
                        continue;

                    int ni = (i + x + GRID_SIZE) % GRID_SIZE;
                    int nj = (j + y + GRID_SIZE) % GRID_SIZE;

                    liveNeighbors += grid[ni][nj];
                }
            }

            if (grid[i][j] == 1)
            {
                if (liveNeighbors < 2 || liveNeighbors > 3)
                    nextGrid[i][j] = 0;
                else
                    nextGrid[i][j] = 1;
            }
            else
            {
                if (liveNeighbors == 3)
                    nextGrid[i][j] = 1;
                else
                    nextGrid[i][j] = 0;
            }
        }
    }

    for (int i = 0; i < GRID_SIZE; i++)
    {
        for (int j = 0; j < GRID_SIZE; j++)
        {
            grid[i][j] = nextGrid[i][j];
        }
    }
}

void DrawGameOfLife()
{
    for (int i = 0; i < GRID_SIZE; i++)
    {
        for (int j = 0; j < GRID_SIZE; j++)
        {
            Color color = grid[i][j] ? HOVER_COLOR : (Color){20, 20, 20, 255};
            DrawRectangle(i * CELL_SIZE, j * CELL_SIZE, CELL_SIZE, CELL_SIZE, color);
        }
    }
}

void AddCellToGrid(int mouseX, int mouseY)
{
    int gridX = mouseX / CELL_SIZE;
    int gridY = mouseY / CELL_SIZE;

    if (gridX >= 0 && gridX < GRID_SIZE && gridY >= 0 && gridY < GRID_SIZE)
    {
        grid[gridX][gridY] = 1;
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                    continue;

                int ni = (gridX + x + GRID_SIZE) % GRID_SIZE;
                int nj = (gridY + y + GRID_SIZE) % GRID_SIZE;

                grid[ni][nj] = 1;
            }
        }
    }
}

void DrawMenu(Font fonts[], MenuOpt menuOptions[], int *popup)
{
    DrawTextEx(fonts[1], LOGO_TEXT, (Vector2){20, 20}, LARGE_FONT_SIZE, 0, PRESSED_COLOR);

    for (int i = 0; i < MENU_OPTIONS_COUNT; i++)
    {
        menuOptions[i].x = 20;
        menuOptions[i].y = 140 + i * 60;
        menuOptions[i].text = menuOptionsNames[i];
        menuOptions[i].clicked = isClicked(menuOptions[i].x, menuOptions[i].y, MeasureText(menuOptions[i].text, 70), MEDIUM_FONT_SIZE, menuOptions[i].clicked);
        int clickedActive = FALSE;
        if (menuOptions[i].clicked)
        {
            *popup = i;
        }
        menuOptions[i].hover = CheckCollisionPointRec(GetMousePosition(), (Rectangle){menuOptions[i].x, menuOptions[i].y, MeasureText(menuOptions[i].text, 70), MEDIUM_FONT_SIZE});

        if (*popup == i)
        {
            clickedActive = TRUE;
        }
        drawMenuOption(menuOptions[i], fonts[1], clickedActive);
    }

    if (*popup != -1)
    {
        int x = 20;
        int y = 500;

        DrawTextEx(fonts[1], menuPopupName[*popup], (Vector2){x, y - 20}, MEDIUM_FONT_SIZE, 0, PRESSED_COLOR);
        SetTextureFilter(GetFontDefault().texture, FILTER_POINT);
        DrawTextEx(fonts[0], menuPopupMessage[*popup], (Vector2){x, y + 30}, SMALL_FONT_SIZE, 0, NORMAL_COLOR);
        menuOptions[*popup].clicked = FALSE;
    }
}
